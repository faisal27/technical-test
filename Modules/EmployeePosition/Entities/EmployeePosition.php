<?php

namespace Modules\EmployeePosition\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeePosition extends Model
{
  use HasFactory, SoftDeletes;

  protected $table = 'employee_positions';
  protected $fillable = ['name'];
}
