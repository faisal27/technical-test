<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\EmployeePosition\Http\Controllers\EmployeePositionController;

Route::prefix('employee-position')->group(function () {
  Route::get('/', [EmployeePositionController::class, 'index'])->name('employee-position.index');
  Route::get('/get', [EmployeePositionController::class, 'edit'])->name('employee-position.get');
  Route::post('/json', [EmployeePositionController::class, 'show'])->name('employee-position.json');
  Route::post('/store', [EmployeePositionController::class, 'store'])->name('employee-position.store');
  Route::put('/update', [EmployeePositionController::class, 'update'])->name('employee-position.update');
  Route::delete('/delete', [EmployeePositionController::class, 'destroy'])->name('employee-position.destroy');
});
