<?php

namespace Modules\EmployeePosition\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\EmployeePosition\Entities\EmployeePosition;
use Modules\EmployeePosition\Http\Requests\EmployeePositionRequest;
use Yajra\DataTables\DataTables;

class EmployeePositionController extends Controller
{
  /**
   * Display a listing of the resource.
   * @return Renderable
   */
  public function index()
  {
    return view('employeeposition::index');
  }


  /**
   * Store a newly created resource in storage.
   * @param EmployeePositionRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function store(EmployeePositionRequest $request)
  {
    $name = $request->name;
    $insert = EmployeePosition::create(['name' => $name]);

    if ($insert) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data berhasil disimpan'
      ]);
    } else {
      return response()->json([
        'status' => 'error',
        'message' => 'Data gagal disimpan'
      ], 400);
    }
  }

  /**
   * Show the specified resource.
   * @return Renderable
   */
  public function show()
  {
    $data = EmployeePosition::query()->get();
    return DataTables::of($data)
      ->addIndexColumn()
      ->addColumn('action', function ($query) {
        return '<a class="btn btn-primary btn-sm" onclick="editData('. $query->id .')"><i class="fa fa-pencil"></i></a> |
                <a class="btn btn-danger btn-sm" onclick="deleteData('. $query->id .')"><i class="fa fa-trash"></i></a>';
      })
      ->make(true);
  }

  /**
   * Show the form for editing the specified resource.
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function edit(Request $request)
  {
    $id = $request->id;
    $data = EmployeePosition::where('id', $id)->first();

    if (is_null($data)) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data tidak ditemukan'
      ], 404);
    }

    return response()->json([
      'status' => 'success',
      'data' => $data
    ]);
  }

  /**
   * Update the specified resource in storage.
   * @param EmployeePositionRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(EmployeePositionRequest $request)
  {
    $id = $request->id;
    $name = $request->name;
    $update = EmployeePosition::where('id', $id)->update(['name' => $name]);

    if ($update) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data berhasil diubah'
      ]);
    } else {
      return response()->json([
        'status' => 'error',
        'message' => 'Data gagal diubah'
      ], 400);
    }
  }

  /**
   * Remove the specified resource from storage.
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy(Request $request)
  {
    $id = $request->id;
    $data = EmployeePosition::where('id', $id)->first();

    if (is_null($data)) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data tidak ditemukan'
      ], 404);
    }

    $delete = $data->delete();
    if ($delete) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data berhasil dihapus'
      ]);
    } else {
      return response()->json([
        'status' => 'error',
        'message' => 'Data gagal dihapus'
      ], 400);
    }
  }
}
