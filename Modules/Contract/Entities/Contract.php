<?php

namespace Modules\Contract\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Employee\Entities\Employee;

class Contract extends Model
{
  use HasFactory, SoftDeletes;

  protected $table = 'contracts';
  protected $fillable = [
    'employee_id',
    'start_join',
    'end_join'
  ];

  public function employee()
  {
    return $this->belongsTo(Employee::class, 'employee_id');
  }
}
