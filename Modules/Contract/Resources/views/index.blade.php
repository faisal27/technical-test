@extends('employeeposition::layouts.master')

@section('content')
   <div class="container-fluid mt-5">
     <div class="row">
       <div class="col-md-12">
         <button class="btn btn-success btn-sm mb-3" onclick="addData()"><i class="fa fa-plus"></i> Tambah</button>

         <table class="table table-bordered table-striped table-responsive" id="contract-table">
           <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Nama Jabatan</th>
                <th>Tanggal Gabung</th>
                <th>Tanggal Berakhir</th>
                <th>Aksi</th>
              </tr>
           </thead>
           <tbody></tbody>
         </table>
       </div>
     </div>
   </div>

   <!-- Modal -->
   <div class="modal fade" id="modal-contract" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <h5 class="modal-title" id="exampleModalLabel">Tambah</h5>
           <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>
         <div class="modal-body">
           <form action="" method="post" id="form-employee">
             <input type="hidden" id="id" name="id">
             <div class="form-group">
               <label for="employee_id">Pegawai<span class="text-danger">*</span></label>
               <select name="employee_id" id="employee_id" class="form-control">
                 @forelse($employees as $employee)
                   <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                 @empty
                   <option value="" disabled>Data belum tersedia</option>
                 @endforelse
               </select>
               <span class="text-danger">
                 <strong id="employee_id-error"></strong>
               </span>
             </div>
             <div class="form-group">
               <label for="start_join">Tanggal Gabung<span class="text-danger">*</span></label>
               <input type="date" class="form-control" id="start_join" name="start_join" placeholder="Masukan tanggal gabung">
               <span class="text-danger">
                 <strong id="start_join-error"></strong>
               </span>
             </div>
             <div class="form-group">
               <label for="end_join">Tanggal Berakhir<span class="text-danger">*</span></label>
               <input type="date" class="form-control" id="end_join" name="end_join" placeholder="Masukan tanggal berakhir">
               <span class="text-danger">
                 <strong id="end_join-error"></strong>
               </span>
             </div>
           </form>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
           <button type="button" class="btn btn-primary" onclick="submitData()">Save</button>
         </div>
       </div>
     </div>
   </div>
@endsection
@push('scripts')
  <script>
    let url, type;
    let table = $('#contract-table').DataTable({
      processing: true,
      serverSide: true,
      responsive: true,
      destroy: true,
      order: [],
      pagingType: "full_numbers",
      lengthMenu: [
        [6, 25, 50, -1],
        [6, 25, 50, "All"]
      ],
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search...",
      },

      ajax: {
        "url": '{{ route('contract.json') }}',
        "type": "Post",
        "headers": {
          "X-CSRF-TOKEN": "{{ csrf_token() }}",
        },
      },

      columns: [
        {data: "DT_RowIndex", orderable: false, searchable: false},
        {data: 'employee.name'},
        {data: 'employee.position.name'},
        {data: 'start_join'},
        {data: 'end_join'},
        {data: 'action', sClass: 'text-center', orderable: false, searchable: false}
      ],
    });

    const addData = function () {
      $('#modal-contract').modal('show');
      url = '{{ route('contract.store') }}';
      type = 'post';
      $('#name').val('');
    }

    const editData = function (id) {
      $('#modal-contract').modal('show');
      url = '{{ route('contract.update') }}';
      type = 'put';

      $.ajax({
        headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
        type: 'get',
        url: '{{ route('contract.get') }}',
        data: {id: id},
        dataType: 'json',
        success: function (response) {
          $('#employee_id').val(response.data.employee_id);
          $('#start_join').val(response.data.start_join);
          $('#end_join').val(response.data.end_join);
          $('#id').val(response.data.id);
        },
        error: function (xhr, error, status) {
          alert(error + ":" + status);
        },
      });
    }

    const deleteData = function (id) {
      Swal.fire({
        title: 'Are you sure?',
        text: "Data yang sudah dihapus tidak dapat dikembalikan.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
            type: 'delete',
            url: '{{ route('contract.destroy') }}',
            data: {id: id},
            dataType: 'json',
            success: function (response) {
              if (response.status === 'success') {
                Swal.fire({
                  icon: 'success',
                  title: 'Berhasil',
                  text: response.message,
                })
                table.ajax.reload();
              } else {
                Swal.fire({
                  icon: 'error',
                  title: 'Gagal',
                  text: response.message,
                })
              }
            },
            error: function (xhr, error, status) {
              alert(error + ":" + status);
            },
          });
        }
      })
    }

    const submitData = function () {
      $.ajax({
        headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
        type: type,
        url: url,
        data: $('#form-employee').serialize(),
        dataType: 'json',
        success: function (response) {
          $('#modal-contract').modal('hide');
          if (response.status === 'success') {
            Swal.fire({
              icon: 'success',
              title: 'Berhasil',
              text: response.message,
            })
            table.ajax.reload();
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Gagal',
              text: response.message,
            })
          }
        },
        error: function (resp) {
          if (_.has(resp.responseJSON, 'errors')) {
            _.map(resp.responseJSON.errors, function (val, key) {
              $('#' + key + '-error').html(val[0]).fadeIn(1000).fadeOut(5000);
            })
          }
          alert(resp.responseJSON.message);
        },
      });
    }
  </script>
@endpush
