<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Contract\Http\Controllers\ContractController;

Route::prefix('contract')->group(function() {
  Route::get('/', [ContractController::class, 'index'])->name('contract.index');
  Route::get('/get', [ContractController::class, 'edit'])->name('contract.get');
  Route::post('/json', [ContractController::class, 'show'])->name('contract.json');
  Route::post('/store', [ContractController::class, 'store'])->name('contract.store');
  Route::put('/update', [ContractController::class, 'update'])->name('contract.update');
  Route::delete('/delete', [ContractController::class, 'destroy'])->name('contract.destroy');
});
