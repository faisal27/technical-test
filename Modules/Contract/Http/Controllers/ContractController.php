<?php

namespace Modules\Contract\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Contract\Entities\Contract;
use Modules\Contract\Http\Requests\ContractRequest;
use Modules\Employee\Entities\Employee;
use Yajra\DataTables\DataTables;

class ContractController extends Controller
{
  /**
   * Display a listing of the resource.
   * @return Renderable
   */
  public function index()
  {
    $employees = Employee::all();
    return view('contract::index', compact('employees'));
  }


  /**
   * Store a newly created resource in storage.
   * @param ContractRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function store(ContractRequest $request)
  {
    $employeeId = $request->employee_id;
    $startJoin = $request->start_join;
    $endJoin = $request->end_join;
    $insert = Contract::create([
      'employee_id' => $employeeId,
      'start_join' => $startJoin,
      'end_join' => $endJoin
    ]);

    if ($insert) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data berhasil disimpan'
      ]);
    } else {
      return response()->json([
        'status' => 'error',
        'message' => 'Data gagal disimpan'
      ], 400);
    }
  }

  /**
   * Show the specified resource.
   * @return Renderable
   * @throws \Exception
   */
  public function show()
  {
    $data = Contract::with('employee.position')->get();
    return DataTables::of($data)
      ->addIndexColumn()
      ->addColumn('action', function ($query) {
        return '<a class="btn btn-primary btn-sm" onclick="editData('. $query->id .')"><i class="fa fa-pencil"></i></a> |
                <a class="btn btn-danger btn-sm" onclick="deleteData('. $query->id .')"><i class="fa fa-trash"></i></a>';
      })
      ->make(true);
  }

  /**
   * Show the form for editing the specified resource.
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function edit(Request $request)
  {
    $id = $request->id;
    $data = Contract::where('id', $id)->first();

    if (is_null($data)) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data tidak ditemukan'
      ], 404);
    }

    return response()->json([
      'status' => 'success',
      'data' => $data
    ]);
  }

  /**
   * Update the specified resource in storage.
   * @param ContractRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(ContractRequest $request)
  {
    $id = $request->id;
    $employeeId = $request->employee_id;
    $startJoin = $request->start_join;
    $endJoin = $request->end_join;
    $update = Contract::where('id', $id)->update([
      'employee_id' => $employeeId,
      'start_join' => $startJoin,
      'end_join' => $endJoin
    ]);

    if ($update) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data berhasil diubah'
      ]);
    } else {
      return response()->json([
        'status' => 'error',
        'message' => 'Data gagal diubah'
      ], 400);
    }
  }

  /**
   * Remove the specified resource from storage.
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy(Request $request)
  {
    $id = $request->id;
    $data = Contract::where('id', $id)->first();

    if (is_null($data)) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data tidak ditemukan'
      ], 404);
    }

    $delete = $data->delete();
    if ($delete) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data berhasil dihapus'
      ]);
    } else {
      return response()->json([
        'status' => 'error',
        'message' => 'Data gagal dihapus'
      ], 400);
    }
  }
}
