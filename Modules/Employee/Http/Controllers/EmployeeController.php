<?php

namespace Modules\Employee\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Contract\Entities\Contract;
use Modules\Employee\Entities\Employee;
use Modules\Employee\Http\Requests\EmployeeRequest;
use Modules\Employee\Http\Requests\UpdateEmployeeRequest;
use Modules\EmployeePosition\Entities\EmployeePosition;
use Yajra\DataTables\DataTables;

class EmployeeController extends Controller
{
  /**
   * Display a listing of the resource.
   * @return Renderable
   */
  public function index()
  {
    $positions = EmployeePosition::all();
    return view('employee::index', compact('positions'));
  }


  /**
   * Store a newly created resource in storage.
   * @param EmployeeRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function store(EmployeeRequest $request)
  {
    $name = $request->name;
    $positionId = $request->position_id;
    $phoneNumber = $request->phone_number;
    $email = $request->email;
    $insert = Employee::create([
      'name' => $name,
      'position_id' => $positionId,
      'phone_number' => $phoneNumber,
      'email' => $email
    ]);

    if ($insert) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data berhasil disimpan'
      ]);
    } else {
      return response()->json([
        'status' => 'error',
        'message' => 'Data gagal disimpan'
      ], 400);
    }
  }

  /**
   * Show the specified resource.
   * @return Renderable
   * @throws \Exception
   */
  public function show()
  {
    $data = Employee::with('position')->get();
    return DataTables::of($data)
      ->addIndexColumn()
      ->addColumn('action', function ($query) {
        $contract = Contract::where('employee_id', $query->id)->first();

        if (is_null($contract)) {
          return '<a class="btn btn-primary btn-sm" onclick="editData('. $query->id .')"><i class="fa fa-pencil"></i></a> |
                <a class="btn btn-danger btn-sm" onclick="deleteData('. $query->id .')"><i class="fa fa-trash"></i></a>';
        } else {
          return '<a class="btn btn-primary btn-sm" onclick="editData('. $query->id .')"><i class="fa fa-pencil"></i></a> |
                <button class="btn btn-danger btn-sm" disabled><i class="fa fa-trash"></i></button>';
        }
      })
      ->make(true);
  }

  /**
   * Show the form for editing the specified resource.
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function edit(Request $request)
  {
    $id = $request->id;
    $data = Employee::where('id', $id)->first();

    if (is_null($data)) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data tidak ditemukan'
      ], 404);
    }

    return response()->json([
      'status' => 'success',
      'data' => $data
    ]);
  }

  /**
   * Update the specified resource in storage.
   * @param UpdateEmployeeRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(UpdateEmployeeRequest $request)
  {
    $id = $request->id;
    $name = $request->name;
    $positionId = $request->position_id;
    $phoneNumber = $request->phone_number;
    $email = $request->email;
    $update = Employee::where('id', $id)->update([
      'name' => $name,
      'position_id' => $positionId,
      'phone_number' => $phoneNumber,
      'email' => $email
    ]);

    if ($update) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data berhasil diubah'
      ]);
    } else {
      return response()->json([
        'status' => 'error',
        'message' => 'Data gagal diubah'
      ], 400);
    }
  }

  /**
   * Remove the specified resource from storage.
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy(Request $request)
  {
    $id = $request->id;
    $data = Employee::where('id', $id)->first();

    if (is_null($data)) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data tidak ditemukan'
      ], 404);
    }

    $delete = $data->delete();
    if ($delete) {
      return response()->json([
        'status' => 'success',
        'message' => 'Data berhasil dihapus'
      ]);
    } else {
      return response()->json([
        'status' => 'error',
        'message' => 'Data gagal dihapus'
      ], 400);
    }
  }
}
