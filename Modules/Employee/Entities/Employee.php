<?php

namespace Modules\Employee\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\EmployeePosition\Entities\EmployeePosition;

class Employee extends Model
{
  use HasFactory, SoftDeletes;

  protected $table = 'employees';
  protected $fillable = [
    'position_id',
    'name',
    'phone_number',
    'email'
  ];

  public function position()
  {
    return $this->belongsTo(EmployeePosition::class, 'position_id');
  }
}
