<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Employee\Http\Controllers\EmployeeController;

Route::prefix('employee')->group(function() {
  Route::get('/', [EmployeeController::class, 'index'])->name('employee.index');
  Route::get('/get', [EmployeeController::class, 'edit'])->name('employee.get');
  Route::post('/json', [EmployeeController::class, 'show'])->name('employee.json');
  Route::post('/store', [EmployeeController::class, 'store'])->name('employee.store');
  Route::put('/update', [EmployeeController::class, 'update'])->name('employee.update');
  Route::delete('/delete', [EmployeeController::class, 'destroy'])->name('employee.destroy');
});
