@extends('employeeposition::layouts.master')

@section('content')
   <div class="container-fluid mt-5">
     <div class="row">
       <div class="col-md-12">
         <button class="btn btn-success btn-sm mb-3" onclick="addData()"><i class="fa fa-plus"></i> Tambah</button>

         <table class="table table-bordered table-striped table-responsive" id="employee-table">
           <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Nama Jabatan</th>
                <th>Nomor Handphone</th>
                <th>Email</th>
                <th>Aksi</th>
              </tr>
           </thead>
           <tbody></tbody>
         </table>
       </div>
     </div>
   </div>

   <!-- Modal -->
   <div class="modal fade" id="modal-employee" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <h5 class="modal-title" id="exampleModalLabel">Tambah</h5>
           <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>
         <div class="modal-body">
           <form action="" method="post" id="form-employee">
             <input type="hidden" id="id" name="id">
             <div class="form-group">
               <label for="name">Name<span class="text-danger">*</span></label>
               <input type="text" class="form-control" id="name" name="name" placeholder="Masukan nama jabatan">
               <span class="text-danger">
                 <strong id="name-error"></strong>
               </span>
             </div>
             <div class="form-group">
               <label for="position_id">Jabatan<span class="text-danger">*</span></label>
               <select name="position_id" id="position_id" class="form-control">
                 @forelse($positions as $position)
                   <option value="{{ $position->id }}">{{ $position->name }}</option>
                 @empty
                   <option value="" disabled>Data belum tersedia</option>
                 @endforelse
               </select>
               <span class="text-danger">
                 <strong id="position_id-error"></strong>
               </span>
             </div>
             <div class="form-group">
               <label for="phone_number">Nomor Handphone<span class="text-danger">*</span></label>
               <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Masukan nomor handphone">
               <span class="text-danger">
                 <strong id="phone_number-error"></strong>
               </span>
             </div>
             <div class="form-group">
               <label for="email">Email<span class="text-danger">*</span></label>
               <input type="text" class="form-control" id="email" name="email" placeholder="Masukan nomor handphone">
               <span class="text-danger">
                 <strong id="email-error"></strong>
               </span>
             </div>
           </form>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
           <button type="button" class="btn btn-primary" onclick="submitData()">Save</button>
         </div>
       </div>
     </div>
   </div>
@endsection
@push('scripts')
  <script>
    let url, type;
    let table = $('#employee-table').DataTable({
      processing: true,
      serverSide: true,
      responsive: true,
      destroy: true,
      order: [],
      pagingType: "full_numbers",
      lengthMenu: [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search...",
      },

      ajax: {
        "url": '{{ route('employee.json') }}',
        "type": "Post",
        "headers": {
          "X-CSRF-TOKEN": "{{ csrf_token() }}",
        },
      },

      columns: [
        {data: "DT_RowIndex", orderable: false, searchable: false},
        {data: 'name'},
        {data: 'position.name'},
        {data: 'phone_number'},
        {data: 'email'},
        {data: 'action', sClass: 'text-center', orderable: false, searchable: false}
      ],
    });

    const addData = function () {
      $('#modal-employee').modal('show');
      url = '{{ route('employee.store') }}';
      type = 'post';
      $('#name').val('');
    }

    const editData = function (id) {
      $('#modal-employee').modal('show');
      url = '{{ route('employee.update') }}';
      type = 'put';

      $.ajax({
        headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
        type: 'get',
        url: '{{ route('employee.get') }}',
        data: {id: id},
        dataType: 'json',
        success: function (response) {
          $('#name').val(response.data.name);
          $('#position_id').val(response.data.position_id);
          $('#phone_number').val(response.data.phone_number);
          $('#email').val(response.data.email);
          $('#id').val(response.data.id);
        },
        error: function (xhr, error, status) {
          alert(error + ":" + status);
        },
      });
    }

    const deleteData = function (id) {
      Swal.fire({
        title: 'Are you sure?',
        text: "Data yang sudah dihapus tidak dapat dikembalikan.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
            type: 'delete',
            url: '{{ route('employee.destroy') }}',
            data: {id: id},
            dataType: 'json',
            success: function (response) {
              if (response.status === 'success') {
                Swal.fire({
                  icon: 'success',
                  title: 'Berhasil',
                  text: response.message,
                })
                table.ajax.reload();
              } else {
                Swal.fire({
                  icon: 'error',
                  title: 'Gagal',
                  text: response.message,
                })
              }
            },
            error: function (xhr, error, status) {
              alert(error + ":" + status);
            },
          });
        }
      })
    }

    const submitData = function () {
      $.ajax({
        headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
        type: type,
        url: url,
        data: $('#form-employee').serialize(),
        dataType: 'json',
        success: function (response) {
          $('#modal-employee').modal('hide');
          if (response.status === 'success') {
            Swal.fire({
              icon: 'success',
              title: 'Berhasil',
              text: response.message,
            })
            table.ajax.reload();
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Gagal',
              text: response.message,
            })
          }
        },
        error: function (resp) {
          if (_.has(resp.responseJSON, 'errors')) {
            _.map(resp.responseJSON.errors, function (val, key) {
              $('#' + key + '-error').html(val[0]).fadeIn(1000).fadeOut(5000);
            })
          }
          alert(resp.responseJSON.message);
        },
      });
    }
  </script>
@endpush
